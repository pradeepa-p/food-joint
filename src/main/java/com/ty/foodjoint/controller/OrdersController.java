package com.ty.foodjoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoint.dto.Orders;
import com.ty.foodjoint.service.OrdersService;
import com.ty.foodjoint.util.ResponseStructure;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class OrdersController {

	@Autowired
	OrdersService ordersService;

	@PostMapping("customer/orders")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Orders"),@ApiResponse(code=404,message = "Orders not Saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Orders>> saveOrder(@RequestBody Orders orders) {
		return ordersService.saveOrder(orders);
	}
	@GetMapping("customer/orders/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Order"),@ApiResponse(code=404,message = "Id not Found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Orders>> getOrderById(@PathVariable int id) {
		return ordersService.getOrderById(id);
	}

	@PutMapping("customer/orders/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Updated Order"),@ApiResponse(code=404,message = "Order not Updated"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Orders>> updateOrder(@RequestBody Orders orders,@PathVariable int id) {
		return ordersService.updateOrder(orders, id);
	}
	
	@DeleteMapping("customer/orders")
	@ApiResponses({@ApiResponse(code=200,message="Deleted Order"),@ApiResponse(code=404,message = "Order not Deleted"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<String>> deleteOrder(@RequestParam int id) {
		return ordersService.deleteOrder(id);
	}

	@GetMapping("customer/orders")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Orders List"),@ApiResponse(code=404,message = "Orders not Retrived"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Orders>>> getAllOrders() {
		return ordersService.getAllOrders();
	}

}
