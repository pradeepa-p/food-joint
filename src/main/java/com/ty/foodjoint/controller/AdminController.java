package com.ty.foodjoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoint.dto.Admin;
import com.ty.foodjoint.dto.Login;
import com.ty.foodjoint.service.AdminService;
import com.ty.foodjoint.util.ResponseStructure;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AdminController {

	@Autowired
	AdminService adminService;

	@PostMapping("/adminlogin")
	@ApiResponses({@ApiResponse(code=200,message="Admin Login Successful"),@ApiResponse(code=404,message = "Admin Login failed"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Admin>> validateAdmin(@RequestBody Login login) {
		return adminService.validateAdmin(login);
	}

	@GetMapping("/admin/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Admin "),@ApiResponse(code=404,message = "Admin not retrived"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Admin>> getAdminById(@PathVariable int id) {
		return adminService.getAdminById(id);
	}

	@PostMapping("/admin")
	@ApiResponses({@ApiResponse(code=200,message="Admin Saved"),@ApiResponse(code=404,message = "Admin not saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Admin>> createAdmin(@RequestBody Admin admin) {
		return adminService.createAdmin(admin);
	}

}
