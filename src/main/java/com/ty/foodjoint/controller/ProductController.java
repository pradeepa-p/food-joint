package com.ty.foodjoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoint.dto.Product;
import com.ty.foodjoint.service.ProductService;
import com.ty.foodjoint.util.ResponseStructure;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ProductController {

	@Autowired
	ProductService productService;

	@PostMapping("admin/product")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Product"),@ApiResponse(code=404,message = "Product not Saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Product>> saveProduct(@RequestBody Product product) {
		return productService.saveProduct(product);
	}

	@GetMapping("admin/product")
	@ApiResponses({@ApiResponse(code=200,message="Retrived All Products"),@ApiResponse(code=404,message = "Products not Retrieved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Product>>> getAllProduct() {
		return productService.getAllProduct();
	}

	@GetMapping("admin/product/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Product"),@ApiResponse(code=404,message = "Id not Found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Product>> getProductById(@PathVariable int id) {
		return productService.getProductById(id);
	}

	@PutMapping("admin/product/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Updated Product"),@ApiResponse(code=404,message = "Product not Saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Product>> updateProduct(@PathVariable int id,
			@RequestBody Product product) {
		return productService.updateProduct(id, product);
	}

	@DeleteMapping("admin/product")
	@ApiResponses({@ApiResponse(code=200,message="Delete Product"),@ApiResponse(code=404,message = "Product not Deleted"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<String>> deleteProduct(@RequestParam int id) {
		return productService.deleteProduct(id);
	}
}
