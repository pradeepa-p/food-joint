package com.ty.foodjoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoint.dto.Item;
import com.ty.foodjoint.service.ItemService;
import com.ty.foodjoint.util.ResponseStructure;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ItemController {

	@Autowired
	ItemService itemService;

	@PostMapping("orders/item")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Item"),@ApiResponse(code=404,message = "Item not Saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Item>> saveItem(@RequestBody Item item) {
		return itemService.saveItem(item);
	}

	@GetMapping("orders/item/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Item"),@ApiResponse(code=404,message = "Item not retrived"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Item>> getItemById(@PathVariable int id) {
		return itemService.getItemById(id);
	}

	@PutMapping("orders/item/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Updated Item"),@ApiResponse(code=404,message = "Item not Updated"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Item>> updateItem(@PathVariable int id, @RequestBody Item item) {
		return itemService.updateItem(id, item);
	}

	@DeleteMapping("orders/items")
	@ApiResponses({@ApiResponse(code=200,message="Deleted Item"),@ApiResponse(code=404,message = "Item not deleted"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<String>> deleteItem(@RequestParam int id) {
		return itemService.deleteItem(id);
	}

	@GetMapping("orders/items")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Item List"),@ApiResponse(code=404,message = "Item List not retrived"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Item>>> getAllItem() {
		return itemService.getAllItem();
	}

}
