package com.ty.foodjoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoint.dto.Customer;
import com.ty.foodjoint.service.CustomerService;
import com.ty.foodjoint.util.ResponseStructure;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@PostMapping("customer")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Customer"),@ApiResponse(code=404,message = "Customer not Saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Customer>> saveCustomer(@RequestBody Customer customer) {
		return customerService.saveCustomer(customer);
	}

	@GetMapping("customer/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Customer"),@ApiResponse(code=404,message = "Id not Found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Customer>> getCustomerById(@PathVariable int id) {
		return customerService.getCustomerById(id);
	}

	@PutMapping("customer/{id}")
	@ApiResponses({@ApiResponse(code=200,message="Updated Customer"),@ApiResponse(code=404,message = "Customer not Updated"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Customer>> updateCustomer(@PathVariable int id,
			@RequestBody Customer customer) {
		return customerService.updateCustomer(id, customer);
	}

	@DeleteMapping("customer")
	@ApiResponses({@ApiResponse(code=200,message="Deleted Customer"),@ApiResponse(code=404,message = "Customer not deleted"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<String>> deleteCustomer(@RequestParam int id) {
		return customerService.deleteCustomer(id);
	}

	@GetMapping("customer")
	@ApiResponses({@ApiResponse(code=200,message="Retrived Customer List"),@ApiResponse(code=404,message = "Customer List not retrived"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Customer>>> getAllCustomer() {
		return customerService.getAllCustomer();
	}

}
