package com.ty.foodjoint.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoint.dto.Login;
import com.ty.foodjoint.dto.User;
import com.ty.foodjoint.service.UserService;
import com.ty.foodjoint.util.ResponseStructure;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
public class UserController {

	@Autowired
	UserService userService;
	
	@PostMapping("admin/user")
	@ApiOperation("To Save User")
	@ApiResponses({@ApiResponse(code=200,message="Retrived User"),@ApiResponse(code=404,message = "User not Saved"),@ApiResponse(code=500,message = "Internal Server Error")})
	public  ResponseEntity<ResponseStructure<User>> saveUser(@RequestBody User user) {
		return userService.saveUser(user);
	}
	
	@GetMapping("admin/user")
	@ApiResponses({@ApiResponse(code=200,message="User List Retrived"),@ApiResponse(code=404,message = "User List Not Found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<User>>> getAllUser() {
		return userService.getAllUser();
	}

	@PostMapping("userlogin")
	@ApiResponses({@ApiResponse(code=200,message="User Validation"),@ApiResponse(code=404,message = "ID not found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public  ResponseEntity<ResponseStructure<User>> validateUser(@RequestBody Login login) {
		return userService.validateUser(login);
	}
	
	@GetMapping("admin/user/{id}")
	@ApiOperation("To Get User By Id")
	@ApiResponses({@ApiResponse(code=200,message="Retrived User"),@ApiResponse(code=404,message = "ID not found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<User>> getUserById(@ApiParam("ID to get User")  @PathVariable ("id")int id) {
	return userService.getUserById(id);
	}
	
	@PutMapping("admin/user")
	@ApiResponses({@ApiResponse(code=200,message="Updated User"),@ApiResponse(code=404,message = "ID not found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<User>> updateUser(@RequestParam int id,@RequestBody User user) {
		return userService.updateUser(id,user);
	}
	
	@DeleteMapping("admin/user")
	@ApiResponses({@ApiResponse(code=200,message="Deleted User"),@ApiResponse(code=404,message = "ID not found"),@ApiResponse(code=500,message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<String>> deleteUser(@RequestParam int id) {
		return userService.deleteUser(id);
	}
	
}
