package com.ty.foodjoint.dao;

import java.util.List;

import com.ty.foodjoint.dto.User;

public interface UserDao {

	User saveUser(User user);
	User updateUser(int id,User user);
	User getUserById(int id);
	List<User> getAllUser();
	User validateUser(String email,String password);
	boolean deleteUser(int id);
	
}
