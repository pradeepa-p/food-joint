package com.ty.foodjoint.dao;

import java.util.List;

import com.ty.foodjoint.dto.Customer;

public interface CustomerDao {

	public Customer saveCustomer(Customer customer);

	public Customer getCustomerById(int id);

	public Customer updateCustomer(int id,Customer customer);

	public boolean deleteCustomer(int id);

	public List<Customer> getAllCustomer();

}
