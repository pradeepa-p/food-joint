package com.ty.foodjoint.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoint.dao.AdminDao;
import com.ty.foodjoint.dto.Admin;
import com.ty.foodjoint.repository.AdminRepository;

@Repository
public class AdminDaoImpl implements AdminDao {

	@Autowired
	AdminRepository adminRepository;

	@Override
	public Admin validateAdmin(String email, String password) {
		return adminRepository.validateAdmin(email, password);
	}

	@Override
	public Admin getAdminById(int id) {
		return adminRepository.getById(id);
	}

	@Override
	public Admin createAdmin(Admin admin) {
		return adminRepository.save(admin);
	}

}
