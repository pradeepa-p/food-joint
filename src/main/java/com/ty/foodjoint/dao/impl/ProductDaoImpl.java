package com.ty.foodjoint.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoint.dao.ProductDao;
import com.ty.foodjoint.dto.Product;
import com.ty.foodjoint.repository.ProductRepository;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	ProductRepository productRepository;

	@Override
	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product updateProduct(int id, Product product) {

		Product existingProduct = getProductById(id);
		if (existingProduct != null) {
			product.setId(id);
			productRepository.save(product);
			return product;
		}
		return null;
	}

	@Override
	public List<Product> getAllProduct() {
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(int id) {
		Optional<Product> optional = productRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public boolean deleteProduct(int id) {
		Product product = getProductById(id);
		if (product != null) {
			productRepository.delete(product);
			return true;
		}
		return false;
	}

}
