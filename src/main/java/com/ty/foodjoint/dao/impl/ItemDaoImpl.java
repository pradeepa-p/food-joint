package com.ty.foodjoint.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoint.dao.ItemDao;
import com.ty.foodjoint.dto.Item;
import com.ty.foodjoint.repository.ItemRepository;

@Repository
public class ItemDaoImpl implements ItemDao{

	@Autowired
	ItemRepository itemRepository;

	@Override
	public Item saveItem(Item item) {
		return itemRepository.save(item);
	}

	@Override
	public Item getItemById(int id) {
		Optional<Item> items = itemRepository.findById(id);
		if (items.isPresent()) {
			return items.get();

		} else
			return null;
	}

	@Override
	public Item updateItem(int id, Item item) {
		Item existingitem = getItemById(id);
		if (existingitem != null) {
			existingitem.setName(item.getName());
			existingitem.setQuantity(item.getQuantity());
			existingitem.setCost(item.getCost());

			return itemRepository.save(existingitem);
		}
		return null;
	}

	@Override
	public boolean deleteItem(int id) {
		Item item = getItemById(id);
		if (item != null) {
			itemRepository.delete(item);
			return true;
		} else
			return false;
	}

	@Override
	public List<Item> getAllItem() {
		return itemRepository.findAll();
	}
}
