package com.ty.foodjoint.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoint.dao.UserDao;
import com.ty.foodjoint.dto.User;
import com.ty.foodjoint.repository.UserRepository;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	UserRepository repository;

	public User saveUser(User user) {
		return repository.save(user);
	}

	public User getUserById(int id) {
		Optional<User> optional = repository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	public List<User> getAllUser() {

		return repository.findAll();
	}

	public boolean deleteUser(int id) {
		User user = getUserById(id);
		if (user != null) {
			repository.delete(user);
			return true;
		}
		return false;
	}

	@Override
	public User updateUser(int id, User user) {
		if (getUserById(id) != null) {
			user.setId(id);
			return repository.save(user);
		}
		return null;
	}

	@Override
	public User validateUser(String email, String password) {
		return repository.validateUser(email, password);
	}

}