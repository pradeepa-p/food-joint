package com.ty.foodjoint.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoint.dao.OrdersDao;
import com.ty.foodjoint.dto.Orders;
import com.ty.foodjoint.repository.OrdersRepository;

@Repository
public class OrdersDaoImpl implements OrdersDao {
	@Autowired
	OrdersRepository orderRepository;

	@Override
	public Orders saveOrder(Orders orders) {
		return orderRepository.save(orders);
	}

	@Override
	public Orders getOrderById(int id) {
		Optional<Orders> orders = orderRepository.findById(id);
		if (orders.isPresent()) {
			return orders.get();

		} else
			return null;
	}

	@Override
	public Orders updateOrder(Orders orders, int id) {
		Orders existingorders = getOrderById(id);
		if (existingorders != null) {
			existingorders.setCustomername(orders.getCustomername());
			existingorders.setUsername(orders.getUsername());
			existingorders.setOffercost(orders.getOffercost());
			existingorders.setTotalcost(orders.getTotalcost());
			existingorders.setStatus(orders.getStatus());

			return orderRepository.save(existingorders);
		}
		return null;
	}

	@Override
	public boolean deleteOrder(int id) {
		Orders orders = getOrderById(id);
		if (orders != null) {
			orderRepository.delete(orders);
			return true;
		} else
			return false;
	}

	@Override
	public List<Orders> getAllOrders() {
		return orderRepository.findAll();
	}}
