package com.ty.foodjoint.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoint.dao.CustomerDao;
import com.ty.foodjoint.dto.Customer;
import com.ty.foodjoint.repository.CustomerRepository;

@Repository
public class CustomerDaoImpl implements CustomerDao {

	@Autowired
	CustomerRepository customerRepository;

	@Override
	public Customer saveCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer getCustomerById(int id) {
		Optional<Customer> optional = customerRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;	}

	@Override
	public Customer updateCustomer(int id, Customer customer) {
		Customer customer2 = getCustomerById(id);
		if (customer2 != null) {
			customer.setId(customer2.getId());
			return customerRepository.save(customer);
		}
		return null;
	}

	@Override
	public boolean deleteCustomer(int id) {
		Customer customer2 = getCustomerById(id);
		if (customer2 != null) {
			customerRepository.delete(customer2);
			return true;
		}
		return false;
	}

	@Override
	public List<Customer> getAllCustomer() {
		return customerRepository.findAll();
	}

}
