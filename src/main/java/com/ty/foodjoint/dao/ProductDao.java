package com.ty.foodjoint.dao;

import java.util.List;

import com.ty.foodjoint.dto.Product;

public interface ProductDao {

	public Product saveProduct(Product product);
	public Product updateProduct(int id,Product product);
	public List<Product> getAllProduct();
	public Product getProductById(int id);
	public boolean deleteProduct(int id);
	
	
}
