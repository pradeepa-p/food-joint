package com.ty.foodjoint.dao;

import com.ty.foodjoint.dto.Admin;

public interface AdminDao {

	public Admin validateAdmin(String email, String password);
	public Admin getAdminById(int id);
	public Admin createAdmin(Admin admin);
}
