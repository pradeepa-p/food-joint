package com.ty.foodjoint.dao;

import java.util.List;

import com.ty.foodjoint.dto.Orders;

public interface OrdersDao {

	public Orders saveOrder(Orders orders);

	public Orders getOrderById(int id);

	public Orders updateOrder(Orders orders, int id);

	public boolean deleteOrder(int id);

	public List<Orders> getAllOrders();

}
