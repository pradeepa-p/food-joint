package com.ty.foodjoint.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ty.foodjoint.util.ResponseStructure;

@ControllerAdvice
public class FoodJointExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(IDNotFoundException.class)
	public ResponseEntity<ResponseStructure<String>> userNotFoundExceptionHandler(IDNotFoundException exception){
		ResponseStructure<String> responseStructure=new ResponseStructure<String>();
		responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
		responseStructure.setMessage(exception.getMessage());
		responseStructure.setData("Exception: ID Not Found");
		return new ResponseEntity<ResponseStructure<String>>(responseStructure, HttpStatus.NOT_FOUND);
	}
	

}
