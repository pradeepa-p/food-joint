package com.ty.foodjoint.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoint.dto.Orders;
import com.ty.foodjoint.util.ResponseStructure;

public interface OrdersService {

	public ResponseEntity<ResponseStructure<Orders>> saveOrder(Orders orders);

	public ResponseEntity<ResponseStructure<Orders>> getOrderById(int id);

	public ResponseEntity<ResponseStructure<Orders>> updateOrder(Orders orders, int id);

	public ResponseEntity<ResponseStructure<String>> deleteOrder(int id);

	public ResponseEntity<ResponseStructure<List<Orders>>> getAllOrders();

}
