package com.ty.foodjoint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoint.Exception.IDNotFoundException;
import com.ty.foodjoint.dao.ItemDao;
import com.ty.foodjoint.dto.Item;
import com.ty.foodjoint.service.ItemService;
import com.ty.foodjoint.util.ResponseStructure;

@Service
public class ItemServiceImpl implements ItemService {
	@Autowired
	ItemDao itemDao;

	@Override
	public ResponseEntity<ResponseStructure<Item>> saveItem(Item item) {
		ResponseStructure<Item> responseStructure = new ResponseStructure<Item>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(itemDao.saveItem(item));
		ResponseEntity<ResponseStructure<Item>> responseEntity = new ResponseEntity<ResponseStructure<Item>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

	}

	@Override
	public ResponseEntity<ResponseStructure<Item>> getItemById(int id) {
		ResponseStructure<Item> responseStructure = new ResponseStructure<Item>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(itemDao.getItemById(id));
		ResponseEntity<ResponseStructure<Item>> responseEntity = new ResponseEntity<ResponseStructure<Item>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Item>> updateItem(int id, Item item) {
		ResponseStructure<Item> responseStructure = new ResponseStructure<Item>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(itemDao.updateItem(id, item));
		ResponseEntity<ResponseStructure<Item>> responseEntity = new ResponseEntity<ResponseStructure<Item>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteItem(int id) {
		if (itemDao.deleteItem(id)) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData("Item Deleted");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("Item Id: " + id + " not found/exists");

		}
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Item>>> getAllItem() {
		ResponseStructure<List<Item>> responseStructure = new ResponseStructure<List<Item>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(itemDao.getAllItem());
		ResponseEntity<ResponseStructure<List<Item>>> responseEntity = new ResponseEntity<ResponseStructure<List<Item>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}
}
