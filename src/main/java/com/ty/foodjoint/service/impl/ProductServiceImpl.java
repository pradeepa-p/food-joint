package com.ty.foodjoint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoint.Exception.IDNotFoundException;
import com.ty.foodjoint.dao.ProductDao;
import com.ty.foodjoint.dto.Product;
import com.ty.foodjoint.service.ProductService;
import com.ty.foodjoint.util.ResponseStructure;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;

	@Override
	public ResponseEntity<ResponseStructure<Product>> saveProduct(Product product) {
		ResponseStructure<Product> responseStructure = new ResponseStructure<Product>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(productDao.saveProduct(product));
		ResponseEntity<ResponseStructure<Product>> responseEntity = new ResponseEntity<ResponseStructure<Product>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

	}

	@Override
	public ResponseEntity<ResponseStructure<Product>> updateProduct(int id, Product product) {

		Product product2 = productDao.updateProduct(id, product);
		if (product2 != null) {
			ResponseStructure<Product> responseStructure = new ResponseStructure<Product>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(product);
			ResponseEntity<ResponseStructure<Product>> responseEntity = new ResponseEntity<ResponseStructure<Product>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("Product Id: " + id + " not found/exists");

		}

	}

	@Override
	public ResponseEntity<ResponseStructure<List<Product>>> getAllProduct() {
		List<Product> products = productDao.getAllProduct();
		ResponseStructure<List<Product>> responseStructure = new ResponseStructure<List<Product>>();
		if (products != null) {
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(products);
			ResponseEntity<ResponseStructure<List<Product>>> responseEntity = new ResponseEntity<ResponseStructure<List<Product>>>(
					responseStructure, HttpStatus.OK);

			return responseEntity;

		} else {
			throw new IDNotFoundException(" No Product found to display");
		}

	}

	@Override
	public ResponseEntity<ResponseStructure<Product>> getProductById(int id) {
		Product product = productDao.getProductById(id);

		if (product != null) {
			ResponseStructure<Product> responseStructure = new ResponseStructure<Product>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(product);
			ResponseEntity<ResponseStructure<Product>> responseEntity = new ResponseEntity<ResponseStructure<Product>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("Product Id: " + id + " not found/exists");

		}

	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteProduct(int id) {
		if (productDao.deleteProduct(id)) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData("Product Deleted");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("Product Id: " + id + " not found/exists");

		}

	}

}
