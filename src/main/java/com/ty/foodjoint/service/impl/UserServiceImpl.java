package com.ty.foodjoint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoint.Exception.IDNotFoundException;
import com.ty.foodjoint.dao.UserDao;
import com.ty.foodjoint.dto.Login;
import com.ty.foodjoint.dto.User;
import com.ty.foodjoint.service.UserService;
import com.ty.foodjoint.util.ResponseStructure;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;

	@Override
	public ResponseEntity<ResponseStructure<User>> saveUser(User user) {
		ResponseStructure<User> responseStructure = new ResponseStructure<User>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(userDao.saveUser(user));
		ResponseEntity<ResponseStructure<User>> responseEntity = new ResponseEntity<ResponseStructure<User>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> updateUser(int id, User users) {

		User user = userDao.updateUser(id, users);
		if (user != null) {
			ResponseStructure<User> responseStructure = new ResponseStructure<User>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(user);
			ResponseEntity<ResponseStructure<User>> responseEntity = new ResponseEntity<ResponseStructure<User>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("User Id: " + id + " not found/exists");
		}

	}

	@Override
	public ResponseEntity<ResponseStructure<User>> getUserById(int id) {

		User user = userDao.getUserById(id);
		if (user != null) {
			ResponseStructure<User> responseStructure = new ResponseStructure<User>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(user);
			ResponseEntity<ResponseStructure<User>> responseEntity = new ResponseEntity<ResponseStructure<User>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("User Id: " + id + " not found/exists");
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<List<User>>> getAllUser() {
		List<User> user = userDao.getAllUser();
		ResponseStructure<List<User>> responseStructure = new ResponseStructure<List<User>>();
		if (user != null) {
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(user);
			ResponseEntity<ResponseStructure<List<User>>> responseEntity = new ResponseEntity<ResponseStructure<List<User>>>(
					responseStructure, HttpStatus.OK);

			return responseEntity;

		} else {
			throw new IDNotFoundException("No User Exists");
		}

	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteUser(int id) {
		boolean delete = userDao.deleteUser(id);
		if (delete) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData("User Deleted");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("User Id: " + id + " not found/exists");

		}
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> validateUser(Login login) {
		ResponseStructure<User> responseStructure = new ResponseStructure<User>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(userDao.validateUser(login.getEmail(), login.getPassword()));
		ResponseEntity<ResponseStructure<User>> responseEntity = new ResponseEntity<ResponseStructure<User>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

}
