package com.ty.foodjoint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoint.Exception.IDNotFoundException;
import com.ty.foodjoint.dao.CustomerDao;
import com.ty.foodjoint.dto.Customer;
import com.ty.foodjoint.service.CustomerService;
import com.ty.foodjoint.util.ResponseStructure;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	CustomerDao customerDao;
	
	@Override
	public ResponseEntity<ResponseStructure<Customer>> saveCustomer(Customer customer) {
		ResponseStructure<Customer> responseStructure = new ResponseStructure<Customer>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(customerDao.saveCustomer(customer));
		ResponseEntity<ResponseStructure<Customer>> responseEntity = new ResponseEntity<ResponseStructure<Customer>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

	
	}

	@Override
	public ResponseEntity<ResponseStructure<Customer>> getCustomerById(int id) {
		ResponseStructure<Customer> responseStructure = new ResponseStructure<Customer>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(customerDao.getCustomerById(id));
		ResponseEntity<ResponseStructure<Customer>> responseEntity = new ResponseEntity<ResponseStructure<Customer>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

		}

	@Override
	public ResponseEntity<ResponseStructure<Customer>> updateCustomer(int id, Customer customer) {
		ResponseStructure<Customer> responseStructure = new ResponseStructure<Customer>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(customerDao.getCustomerById(id));
		ResponseEntity<ResponseStructure<Customer>> responseEntity = new ResponseEntity<ResponseStructure<Customer>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

	
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteCustomer(int id) {
		if (customerDao.deleteCustomer(id)) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData("Customer Deleted");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("Customer Id: " + id + " not found/exists");

		}


		}

	@Override
	public ResponseEntity<ResponseStructure<List<Customer>>> getAllCustomer() {
		ResponseStructure<List<Customer>> responseStructure = new ResponseStructure<List<Customer>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(customerDao.getAllCustomer());
		ResponseEntity<ResponseStructure<List<Customer>>> responseEntity = new ResponseEntity<ResponseStructure<List<Customer>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;	}

}
