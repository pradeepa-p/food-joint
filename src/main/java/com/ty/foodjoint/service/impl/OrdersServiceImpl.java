package com.ty.foodjoint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoint.Exception.IDNotFoundException;
import com.ty.foodjoint.dao.OrdersDao;
import com.ty.foodjoint.dto.Orders;
import com.ty.foodjoint.service.OrdersService;
import com.ty.foodjoint.util.ResponseStructure;

@Service
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	OrdersDao ordersDao;

	@Override
	public ResponseEntity<ResponseStructure<Orders>> saveOrder(Orders orders) {
		ResponseStructure<Orders> responseStructure = new ResponseStructure<Orders>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(ordersDao.saveOrder(orders));
		ResponseEntity<ResponseStructure<Orders>> responseEntity = new ResponseEntity<ResponseStructure<Orders>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Orders>> getOrderById(int id) {
		ResponseStructure<Orders> responseStructure = new ResponseStructure<Orders>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(ordersDao.getOrderById(id));
		ResponseEntity<ResponseStructure<Orders>> responseEntity = new ResponseEntity<ResponseStructure<Orders>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Orders>> updateOrder(Orders orders, int id) {
		ResponseStructure<Orders> responseStructure = new ResponseStructure<Orders>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(ordersDao.updateOrder(orders, id));
		ResponseEntity<ResponseStructure<Orders>> responseEntity = new ResponseEntity<ResponseStructure<Orders>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteOrder(int id) {
		if (ordersDao.deleteOrder(id)) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData("Orders Deleted");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;

		} else {
			throw new IDNotFoundException("Orders Id: " + id + " not found/exists");

		}

	}

	@Override
	public ResponseEntity<ResponseStructure<List<Orders>>> getAllOrders() {
		ResponseStructure<List<Orders>> responseStructure = new ResponseStructure<List<Orders>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(ordersDao.getAllOrders());
		ResponseEntity<ResponseStructure<List<Orders>>> responseEntity = new ResponseEntity<ResponseStructure<List<Orders>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

}
