package com.ty.foodjoint.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoint.dto.Customer;
import com.ty.foodjoint.util.ResponseStructure;

public interface CustomerService {
	public ResponseEntity<ResponseStructure<Customer>> saveCustomer(Customer customer);

	public ResponseEntity<ResponseStructure<Customer>> getCustomerById(int id);

	public ResponseEntity<ResponseStructure<Customer>> updateCustomer(int id, Customer customer);

	public ResponseEntity<ResponseStructure<String>> deleteCustomer(int id);

	public ResponseEntity<ResponseStructure<List<Customer>>> getAllCustomer();

}
