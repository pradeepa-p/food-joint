package com.ty.foodjoint.service;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoint.dto.Admin;
import com.ty.foodjoint.dto.Login;
import com.ty.foodjoint.util.ResponseStructure;

public interface AdminService {

	public ResponseEntity<ResponseStructure<Admin>> validateAdmin(Login login);
	public ResponseEntity<ResponseStructure<Admin>> getAdminById(int id);
	public ResponseEntity<ResponseStructure<Admin>> createAdmin(Admin admin);

}
