package com.ty.foodjoint.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoint.dto.Product;
import com.ty.foodjoint.util.ResponseStructure;

public interface ProductService {
	public ResponseEntity<ResponseStructure<Product>> saveProduct(Product product);
	public ResponseEntity<ResponseStructure<Product>> updateProduct(int id,Product product);
	public ResponseEntity<ResponseStructure<List<Product>>> getAllProduct();
	public ResponseEntity<ResponseStructure<Product>> getProductById(int id);
	public ResponseEntity<ResponseStructure<String>> deleteProduct(int id);
	
}
