package com.ty.foodjoint.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoint.dto.Item;
import com.ty.foodjoint.util.ResponseStructure;

@Service
public interface ItemService {

	public ResponseEntity<ResponseStructure<Item>> saveItem(Item item);

	public ResponseEntity<ResponseStructure<Item>> getItemById(int id);

	public ResponseEntity<ResponseStructure<Item>> updateItem(int id, Item item);

	public ResponseEntity<ResponseStructure<String>> deleteItem(int id);

	public ResponseEntity<ResponseStructure<List<Item>>> getAllItem();

}
