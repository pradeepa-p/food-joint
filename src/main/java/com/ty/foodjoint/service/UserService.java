package com.ty.foodjoint.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoint.dto.Login;
import com.ty.foodjoint.dto.User;
import com.ty.foodjoint.util.ResponseStructure;

public interface UserService {
	public ResponseEntity<ResponseStructure<User>> saveUser(User user);

	public ResponseEntity<ResponseStructure<User>> updateUser(int id, User user);

	public ResponseEntity<ResponseStructure<User>> getUserById(int id);

	public ResponseEntity<ResponseStructure<List<User>>> getAllUser();

	public ResponseEntity<ResponseStructure<String>> deleteUser(int id);

	public ResponseEntity<ResponseStructure<User>> validateUser(Login login);
}
