package com.ty.foodjoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.foodjoint.dto.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
