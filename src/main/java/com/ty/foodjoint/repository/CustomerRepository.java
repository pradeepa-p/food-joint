package com.ty.foodjoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.foodjoint.dto.Customer;

public interface CustomerRepository  extends JpaRepository<Customer, Integer>{

}
