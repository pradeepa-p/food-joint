package com.ty.foodjoint.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ty.foodjoint.FoodJointApplication;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class FoodJointConfiguration {

	@Bean
	public Docket getDocket() {
	
		Contact contact=new Contact("Alpha Team", "https://testyantra.com/", "alpha@ty.com");
		List<VendorExtension> extensions=new ArrayList<VendorExtension>();
		
		ApiInfo apiInfo=new ApiInfo("Food Joint API Document", "API's to place the order to customers in house", "TYP-FoodJoint-Snapshoot 1.0.1", "https://testyantra.com/", contact, "License 11001", "www.google.com", extensions);
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.ty.foodjoint"))
				.build()
		.apiInfo(apiInfo)
		.useDefaultResponseMessages(false);
	
	}
}
